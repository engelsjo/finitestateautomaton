"""
CIS/MTH 465 Finite State Automaton Project
@version: 1.0
@author(s): Adam Terwilliger, Josh Engelsma
@date: 09/16/2015
"""

import sys
import os

class Node:
	def __init__(self):
		node_id = None
		non_57 = None
		a5 = None
		a7 = None

class Automaton:

	def __init__(self):
		"""
		initialize our structure
		"""
		self.nodes = []
		a5_paths = [1, 3, 4, 3, 6, 7, 6, 9, 10, 9, 12, 11, 12]
		a7_paths = [2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 11, 11, 11]
		
		#create new nodes
		for i in range(13):
			new_node = Node()
			new_node.node_id = i
			self.nodes.append(new_node)
		
		#set up the links between nodes
		for j in range(13):
			self.nodes[j].non_57 = self.nodes[j]
			self.nodes[j].a5 = self.nodes[a5_paths[j]]
			self.nodes[j].a7 = self.nodes[a7_paths[j]]

	def isInLanguage(self, stringToProcess):
		"""
		verify that the string passed is in fact in our language
		"""
		for char in stringToProcess:
			if ord(char) < 48 or ord(char) > 55:
				return False
		return True

def processFromFile(aFile):
	with open(aFile, "r") as fh:
		for line in fh:
			processString(line)
			
def processString(aString):
	stringToProcess = aString.strip() #remove leading and trailing whitespace
	machine = Automaton()
	machine_nodes = machine.nodes
	curr_node = machine_nodes[0]
	accept_node = machine_nodes[12]
	
	#verify we have a valid string to process
	if not machine.isInLanguage(stringToProcess):
		print("All characters in str must be in {0, 1, 2, 3, 4, 5, 6, 7}")
		sys.exit(-9)
	
	for i in stringToProcess:
		print("In state {}".format(str(curr_node.node_id)))
		current_char = int(i)
		if current_char == 7:
			curr_node = curr_node.a7
		elif current_char == 5:
			curr_node = curr_node.a5
		elif current_char in [0,1,2,3,4,6]:
			curr_node = curr_node.non_57
	
	print("\nThe final state of string {} is {}".format(stringToProcess, curr_node.node_id))
	
	if curr_node.node_id == accept_node.node_id:
		print("\nThis string is ACCEPTED!!!!")
	else:
		print("\nThis string is not accepted...:(")
		if curr_node.node_id in [0,1,2,4,5,7]:
			print ("Not enough 5s or 7s!")
		elif curr_node.node_id in [3,6,9]:
			print ("Not enough 7s!")
		elif curr_node.node_id in [8,10]:
			print ("Not enough 5s!")
		elif curr_node.node_id == 11:
		  	print("Too many 7s!")


def main(argv):
	if (len(argv) < 3):
		print("You must pass in a switch (-f or -c) and a valid string or filepath")
		usage()
		sys.exit(-1)
	if argv[1] != "-f" and argv[1] != "-c":
		print("you passed an invalid switch.. exiting...")
		usage()
		sys.exit(-2)
	if argv[1] == "-f":
		#process from file
		if not os.path.isfile(argv[2]):
			print("you must pass the path to a file {} is not a file".format(argv[2]))
			sys.exit(-3)
		else:
			fileOfStrings = argv[2]
			processFromFile(fileOfStrings)
			sys.exit(0)
	else:
		#process from cmd line
		stringToProcess = argv[2]
		processString(stringToProcess)
		sys.exit(0)

def usage():
	str_useage = """
	To read strings from file use the following syntax
	python Automaton.py -f filename

	To read strings from the command line use the following syntax
	python Automaton.py -c string 
	"""
	print(str_useage)

if __name__ == "__main__":
	main(sys.argv)
