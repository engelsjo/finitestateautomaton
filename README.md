README for MTH 465 - Automata and Theory of Computation -- Programming Project 1

Developers: Adam Terwilliger and Joshua Engelsma

Version: 10/8/2015

Summary: Implement Finite State Machine for the regular language -- R_2 = {w|w has at least two 5s and exactly three 7s}, with the alphabet {0,1,2,3,4,5,6,7}

Our code references the following informal finite state diagrams:

![MTH_465_FSM_PP_update.png](https://bitbucket.org/repo/eXpeA7/images/1257678837-MTH_465_FSM_PP_update.png) 

![MTH_465_DFM_withNodeIDs.png](https://bitbucket.org/repo/eXpeA7/images/813953638-MTH_465_DFM_withNodeIDs.png)

Contact: terwilla@mail.gvsu.edu